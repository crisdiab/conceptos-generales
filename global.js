"use strict";
// var, const, let
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
exports.__esModule = true;
// import {Nombre} from 'directorio'
var numero; //declarada
var numeroInicial = 3.5; // inicializada
var bandera;
var nombre;
var cualquierCosa; // es un pecado
var arreglo = [4, 5];
var arregloNumerico;
arregloNumerico = [4, 6, 7, 89];
var arregloLetras = ['fasdf', 3, true];
var objetoPersona = {
    nombre: 'fasdf',
    edad: 3
};
objetoPersona.direccion = 'direccion';
var multiplicar1 = function () { return 4 * 4; };
var multiplicar2 = function () {
    var a = 5;
    return 4 * 4 + a;
};
console.log('multiplicar', multiplicar1());
console.log('multiplicar', multiplicar2());
var multiplicarConParametros = function (parametro1, parametro2) {
    return parametro1 * parametro2;
};
multiplicarConParametros(4, 5);
console.log('multiplicar con parametros', multiplicarConParametros(4, 5));
var multiplicarConParametros2 = function (parametro1, parametro2) {
    if (parametro1 === void 0) { parametro1 = 5; }
    if (parametro2 === void 0) { parametro2 = 3; }
    return parametro1 * parametro2;
};
console.log('multiplicar con parametros', multiplicarConParametros2(2, 10));
var multiplicarConParametros3 = function (parametro1, parametro2) {
    if (parametro1 === void 0) { parametro1 = 5; }
    parametro2 = parametro2 ? parametro2 : 20;
    return parametro1 * parametro2;
};
console.log('opcional', multiplicarConParametros3(1));
var inyeccionParametros = function (parametro, parametro2) {
    console.log('parametros que llegan', parametro, parametro2);
};
console.log('inyeccion parametros', inyeccionParametros(1));
var persona1 = {
    nombre: 'asdfa',
    apellido: 'fgggg'
};
var edad = 4;
var persona2 = {
    persona1: persona1,
    edad: edad
};
var persona3 = __assign(__assign({}, persona1), { edad: edad });
var persona4 = {
    nombre: persona1.nombre,
    apellido: persona1.apellido,
    direccion: persona1.direccion,
    edad: edad
};
persona1.direccion = 'fasdf';
//{
// persona1 :{
//    nombre: 'asdfa',
//     apellido: 'fgggg'},
// edad: 4
// }
// console.log('persona 1', persona1)
// console.log('persona 2', persona2)
// console.log('persona 3', persona3)
var arreglo1 = [1, 2, 3];
var arreglo2 = [4, 5, 6];
var arreglo3 = __spreadArray(__spreadArray([], arreglo1, true), arreglo2, true);
console.log('arreglo1', arreglo1);
console.log('arreglo2', arreglo2);
console.log('arreglo3', arreglo3);
var clon1 = JSON.stringify(objetoPersona); // convierte en un string
var objetoClon1 = JSON.parse(clon1);
// @ts-ignore
var clon2 = Object.assign({}, persona1);
delete persona1.direccion;
var llavesObjeto = Object.keys(persona1); // arreglo de las keys del objeto
console.log('llaves objeto', llavesObjeto);
console.log('persona1', persona1);
console.log('clon1', typeof clon1);
console.log('objeclon1', typeof objetoClon1);
console.log('objclon1', objetoClon1);
console.log('objclon2', clon2);
