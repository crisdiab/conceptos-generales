var arregloNumeros = [10, 20, 30, 40];
// Metodos
arregloNumeros.push(5); // agregar al final
arregloNumeros.pop(); // elimina el ultimo
arregloNumeros[10] = 8; // [1,2,3,4,undefined,undefined,undefined,undefined,10]
arregloNumeros.unshift(0); // agregar al inicio
var indice = arregloNumeros.indexOf(10);
console.log('indice', indice);
var arregloSlice = arregloNumeros.slice(arregloNumeros.indexOf(20), arregloNumeros.indexOf(40)); // [0,10]
var arregloSliceClonar = arregloNumeros.slice(0); // otra manera de clonar
console.log('arregloslice', arregloSlice);
//[0,10,20,30,40]
var arregloSplice = arregloNumeros.splice(2, 1);
console.log('arreglo splice', arregloSplice);
arregloNumeros = arregloNumeros.splice(1, 1);
console.log('arreglo numeros', arregloNumeros);
