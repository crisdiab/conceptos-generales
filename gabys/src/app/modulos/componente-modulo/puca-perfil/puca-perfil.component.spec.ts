import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PucaPerfilComponent } from './puca-perfil.component';

describe('PucaPerfilComponent', () => {
  let component: PucaPerfilComponent;
  let fixture: ComponentFixture<PucaPerfilComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PucaPerfilComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PucaPerfilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
