import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GabysPerfilComponent } from './gabys-perfil.component';

describe('GabysPerfilComponent', () => {
  let component: GabysPerfilComponent;
  let fixture: ComponentFixture<GabysPerfilComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GabysPerfilComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GabysPerfilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
