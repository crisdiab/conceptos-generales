import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CristianPerfilComponent } from './cristian-perfil.component';

describe('CristianPerfilComponent', () => {
  let component: CristianPerfilComponent;
  let fixture: ComponentFixture<CristianPerfilComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CristianPerfilComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CristianPerfilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
