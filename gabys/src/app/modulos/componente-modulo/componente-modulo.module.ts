import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GabysPerfilComponent } from './gabys-perfil/gabys-perfil.component';
import { CristianPerfilComponent } from './cristian-perfil/cristian-perfil.component';
import { PucaPerfilComponent } from './puca-perfil/puca-perfil.component';



@NgModule({
  declarations: [GabysPerfilComponent, CristianPerfilComponent, PucaPerfilComponent],
  imports: [
    CommonModule
  ],
  providers:[],
  exports: [GabysPerfilComponent]
})
export class ComponenteModuloModule { }
