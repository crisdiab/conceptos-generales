import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { EjemploComponent } from './componentes/ejemplo/ejemplo.component';
import {ComponenteModuloModule} from './modulos/componente-modulo/componente-modulo.module';

// decorador
@NgModule(
  {
    declarations: [
      AppComponent,
      EjemploComponent
    ], // declarar todos los componentes que yo cree
    imports: [
      BrowserModule,
      ComponenteModuloModule
    ], // agregar todos los modulos que yo voy a utilizar
    providers: [], // agregar todos los servicios que yo voy a utilizar
    bootstrap: [AppComponent]
  }
)
export class AppModule {}
