// var, const, let

import {PersonaInterface} from './persona.interface';
// import {Nombre} from 'directorio'
let numero: number; //declarada
let numeroInicial: number = 3.5; // inicializada
let bandera: boolean;
let nombre: string;
let cualquierCosa: any // es un pecado


let arreglo = [4,5];
let arregloNumerico : number[] ;
arregloNumerico = [4,6,7,89];
let arregloLetras: Array<string | number | boolean> = ['fasdf', 3, true];

const objetoPersona: PersonaInterface = {
    nombre: 'fasdf',
    edad: 3
}
 objetoPersona.direccion = 'direccion';



const multiplicar1 = () => 4*4;
const multiplicar2 = () => {
    let a = 5
    return 4*4 + a;
}
console.log('multiplicar', multiplicar1());
console.log('multiplicar', multiplicar2());

const multiplicarConParametros = (parametro1: number,parametro2: number) => {
    return parametro1 * parametro2;
}

multiplicarConParametros(4,5);
console.log('multiplicar con parametros', multiplicarConParametros(4,5))

const multiplicarConParametros2 = (parametro1 = 5 ,parametro2= 3) => {
    return parametro1 * parametro2;
}

console.log('multiplicar con parametros', multiplicarConParametros2(2,10))


const multiplicarConParametros3 = (parametro1 = 5 ,parametro2?: number) => {
    parametro2 = parametro2 ? parametro2 : 20;
    return parametro1 * parametro2;
}


console.log('opcional', multiplicarConParametros3(1))

const inyeccionParametros = (parametro, parametro2?) => {
    console.log('parametros que llegan', parametro, parametro2);
}
console.log('inyeccion parametros', inyeccionParametros(1))

const persona1: PersonaInterface = {
    nombre: 'asdfa',
    apellido: 'fgggg'
}

const edad = 4;
const persona2: any = {
    persona1,
    edad
}
const persona3 = {
    ...persona1,
    edad
}
const persona4 = {
    nombre: persona1.nombre,
    apellido: persona1.apellido,
    direccion: persona1.direccion,
    edad
}
persona1.direccion = 'fasdf'
//{
// persona1 :{
//    nombre: 'asdfa',
//     apellido: 'fgggg'},
// edad: 4
// }

// console.log('persona 1', persona1)
// console.log('persona 2', persona2)
// console.log('persona 3', persona3)

const arreglo1 = [1,2,3];
const arreglo2 = [4,5,6];
const arreglo3 = [...arreglo1,...arreglo2];
console.log('arreglo1', arreglo1)
console.log('arreglo2',arreglo2)
console.log('arreglo3', arreglo3)
const clon1 = JSON.stringify(objetoPersona) // convierte en un string
const persona5 = persona1;
const objetoClon1 = JSON.parse(clon1);
// @ts-ignore
const clon2 = Object.assign({},persona1);
// @ts-ignore
const clon3 = Object.assign([],arreglo1);
delete persona1.direccion;
const llavesObjeto = Object.keys(persona1); // arreglo de las keys del objeto

console.log('llaves objeto', llavesObjeto);
console.log('persona1', persona1);
console.log('clon1',typeof clon1);
console.log('objeclon1',typeof objetoClon1);
console.log('objclon1',objetoClon1);
console.log('objclon2',clon2);





