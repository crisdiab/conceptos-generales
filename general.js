// var Es una declaracion global,
// let Es una declaracion interna,
// const Es una declaracion en la que el valor jamas va a cambiar

var nombre = "Cristian" // no se debe usar

nombre = "GAby"

let apellido = "LAra"

apellido = 435

let a= 4
let b = "d"

let c = a + b

let numero = 5;
let caracteres = "esto es una cadena";
let caracteres2 = 'esto es lo mimso';
let nombreCompleto = nombre + ' ' + apellido; // GAby LAra
let nombreCompleto2 = `${nombre} ${apellido}--- \n ${numero}`;
//
// console.log('numero', numero);
// console.log('caracteres', caracteres);
// console.log('caracteres2', caracteres2);
// console.log('nombre completo 1', nombreCompleto);
// console.log('nombre completo 2', nombreCompleto2);

const decimal = 10.4;
let banderas = false;
const arreglos = [4,5,6,7];
const arregloStrings = ['a', 'b', 'c'];
const arregloMixto = ['a', 3, false, ['s', 0, ['g']]];

const objetoPersona = {
    nombre: 'Cristian',
    edad: 32,
};

//Agregar atributos nuevos
objetoPersona.tieneMascotas = true;
objetoPersona['direccion'] = 'Al norte todo norte';


sumar();
console.log('sumar', sumar());
function sumar(){
    return 5 + 4;
}

const funcionSumar = sumar;

console.log('funcion sumar definicion', funcionSumar);
console.log('funcion sumar definicion ejecucion', funcionSumar());

const funcionRestar = function () {
    return 5 - 1;
}

funcionRestar();

console.log('restar definicion', funcionRestar);
console.log('restar ejecucion', funcionRestar());
console.log('restar ejecucion', funcionRestar());

const multiplicar1 = () => 4*4;
const multiplicar2 = () => {
    let a = 5
    return 4*4 + a;
}
console.log('multiplicar', multiplicar1());
console.log('multiplicar', multiplicar2());
