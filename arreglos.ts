let arregloNumeros = [10,20,30,40];
// Metodos
arregloNumeros.push(5); // agregar al final
arregloNumeros.pop(); // elimina el ultimo
arregloNumeros[10] = 8; // [1,2,3,4,undefined,undefined,undefined,undefined,10]
arregloNumeros.unshift(0); // agregar al inicio
const indice = arregloNumeros.indexOf(10);
console.log('indice',indice )
const arregloSlice = arregloNumeros.slice(arregloNumeros.indexOf(20),arregloNumeros.indexOf(40)); // [0,10]
const arregloSliceClonar = arregloNumeros.slice(0); // otra manera de clonar
console.log('arregloslice', arregloSlice)
//[0,10,20,30,40]
const arregloSplice = arregloNumeros.splice(2,1);
console.log('arreglo splice', arregloSplice)
arregloNumeros.splice(1,1)
console.log('arreglo numeros', arregloNumeros)


