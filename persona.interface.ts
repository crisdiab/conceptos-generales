export interface PersonaInterface {
    nombre?: string,
    edad?: number;
    apellido?: string;
    direccion?: string;
}
