# NPM pagina
https://www.npmjs.com/

# Markdown 

https://www.markdownguide.org/cheat-sheet/
## requisitos
- Tener instalado nodejs en el computador
- Tener instalado el paquete angular cli
  ```
  npm install -g @angular/cli
  ```
# Crear proyecto angular

```
ng new nombreProyecto
```

# instalar paquetes
Verificar que se encuentre en el directorio con el package.json
npm install
npm i

# Levantar proyecto

## forma 1
Se utiliza cuando el angular cli no esta instalado
npm run start

## forma 2

ng serve 
ng s

Para levantar en otro puerto
ng s --port 4201


## generar un componente

ng generate componente nombreComponente
ng g c NombreComponente


## generar un modulo

ng generate module nombreModulo
ng g module nombreModulo
